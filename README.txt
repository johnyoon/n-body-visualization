This is a documentation for AFRL Team1's Project for 
UCLA CS130 Software Engineering

Team Members: Trent Beamer, Bobby Kazimiroff, John Yoon, Yixin Zhu
Project Initialization Date: October 23, 2012

Notes from Bobby:
Additional Packages that will need to be installed for OpenGL stuff, some of these may have been done already in order to install the CUDA Samples. Libglew-dev below should end up being libglew1.6-dev I believe.

  (sudo  apt-get install) 
	libglew-dev 
	freeglut3-dev 
	libglm-dev

Handy Website for OpenGL basics:
http://en.wikibooks.org/wiki/OpenGL_Programming#Modern_OpenGL
