uniform vec4 fcolor;
varying float depth;
void main( )
{
	float depthMod = depth;
	gl_FragColor = vec4(fcolor.x - 0.5 * depthMod, fcolor.y - 0.5 * depthMod, 0.0 ,1.0);
}
