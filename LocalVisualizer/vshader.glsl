in vec4 vPosition;
uniform mat4 WorldView;
uniform mat4 Proj;
varying float depth;
void main( )
{
	vec4 position = vec4(vPosition.xyz, 1.0);
	vec4 finalPos = vPosition * WorldView * Proj; 
	depth = finalPos.z / finalPos.w;
	gl_Position = vPosition * WorldView * Proj;
}
