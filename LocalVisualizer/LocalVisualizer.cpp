/**
 * From the OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glew.h>

#include <GL/glut.h>
#include <GL/glext.h>
/* GLM */
// #define GLM_MESSAGES
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
#include "../CommonGraphics/shader_utils.h"
//Angel.h provides matrix calculations, vec objects
#include "../CommonGraphics/Angel.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include <cstring> // Memset
#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */
#include <time.h>
#include <errno.h>

#define ECHO_PORT          (2002)
#define MAX_LINE           (100000)
#define LISTENQ            (1024) 


int screen_width=640, screen_height=480;


/* Globals for render-to-texture */
GLuint fbo, fbo_texture, rbo_depth;

GLuint program;

//The following two lines are for the program/shaders that will combine multiple separate renders together
GLuint vbo_fbo_vertices;
GLuint program_postproc, attribute_v_coord_postproc, uniform_fbo_texture, uniform_fbo_texture_other;

GLuint vArray, buffer;
GLuint loc;


//TODO
//Allow for any number of particles to come through the buffer at once, instead of just 1024
float particles[4096];

//TODO
//Allow for any screen resolution by making this depth result buffer dynamically allocated
GLfloat depthResult[640*480];

mat4 world;
mat4 view;
mat4 proj;

vec4 eyePosition = vec4(0,0,25,0);
vec4 eyeDirection = vec4(0,0,-1,0);
vec4 perpEyeDirection = vec4(1,0,0,0);

vec4 upVec = vec4(0,1,0,0);

float aspectRat = 640.0/480.0;

int xAngle = 270;

const float PI = 3.1415926535897932384626;

//constants for socket communication
fd_set 		fds;			//FD_Set used for select(), so we can delay
struct timeval tv;

int       	list_s;                			//  listening socket          
int       	conn_s;                			//  connection socket   

int		sendToSocket;      			//  Combining Visualizer socket
int 		feedbackSocket;

short int	fbPort;
short int 	toPort;		       			//  port number of target machine
short int 	port;                  			//  port number   
            
struct    	sockaddr_in servaddr, servaddr_other;  	//  socket address structure
struct    	sockaddr_in sendToServaddr, fbaddr;

char     	*endptr;                		//  for strtol()              

int 		socklen, fbsocklen;


void error(char *msg)
{
    perror(msg);
    exit(1);
}


//creates shaders for particle system, the program to run, and sets up buffers with particle data
//This will eventually need to be split up further, so that the shader creation occurs only once
//But the bufferes can be reset for new values.
int initParticleSystem()
{
	GLint link_ok = GL_FALSE;
	GLint validate_ok = GL_FALSE;

  	GLuint vs, fs;
	if ((vs = create_shader("vshader.glsl", GL_VERTEX_SHADER))   == 0) return 0;
	if ((fs = create_shader("fshader.glsl", GL_FRAGMENT_SHADER)) == 0) return 0;

	program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
	if (!link_ok) 
	{
		fprintf(stderr, "glLinkProgram:");
		print_log(program);
		return 0;
	}

	glUseProgram(program);

	
	//Set up framebuffer for offscreen rendering
	/* Create back-buffer, used for post-processing */
 
 	/* Texture */
  	glActiveTexture(GL_TEXTURE0);
  	glGenTextures(1, &fbo_texture);
  	glBindTexture(GL_TEXTURE_2D, fbo_texture);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screen_width, screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  	glBindTexture(GL_TEXTURE_2D, 0);
 
  	/* Depth buffer */
  	glGenRenderbuffers(1, &rbo_depth);
  	glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth);
  	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, screen_width, screen_height);
  	glBindRenderbuffer(GL_RENDERBUFFER, 0);
 
  	/* Framebuffer to link everything together */
  	glGenFramebuffers(1, &fbo);
  	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0);
  	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo_depth);
  	GLenum status;
  	if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
    		fprintf(stderr, "glCheckFramebufferStatus: error %d", status);
    		return 0;
  	}
  	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//Create vertext array for second mode (combining screencaptures)
	GLfloat fbo_vertices[] = {
    				-1, -1,
    				 1, -1,
    				-1,  1,
    				 1,  1,
  				};

  	glGenBuffers(1, &vbo_fbo_vertices);
  	glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
  	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);
  	glBindBuffer(GL_ARRAY_BUFFER, 0);

 	//Create and bind the vertex array we'll be using
	glGenVertexArrays(1, &vArray);
	glBindVertexArray(vArray);

	//Create/bind the buffer for the vertex data
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);


	/* Post-processing */
  	if ((vs = create_shader("postproc.v.glsl", GL_VERTEX_SHADER))   == 0) return 0;
  	if ((fs = create_shader("postproc.f.glsl", GL_FRAGMENT_SHADER)) == 0) return 0;
 
  	program_postproc = glCreateProgram();
  	glAttachShader(program_postproc, vs);
  	glAttachShader(program_postproc, fs);
  	glLinkProgram(program_postproc);
  	glGetProgramiv(program_postproc, GL_LINK_STATUS, &link_ok);
  	if (!link_ok) {
  	  fprintf(stderr, "glLinkProgram:");
  	  print_log(program_postproc);
  	  return 0;
  	}
  	glValidateProgram(program_postproc);
  	glGetProgramiv(program_postproc, GL_VALIDATE_STATUS, &validate_ok); 
  	if (!validate_ok) {
  	  fprintf(stderr, "glValidateProgram:");
  	  print_log(program_postproc);
  	}
 	
  	attribute_v_coord_postproc = glGetAttribLocation(program_postproc, "v_coord");
  	if (attribute_v_coord_postproc == -1) {
  	  fprintf(stderr, "Could not bind attribute v_coord\n");
  	  return 0;
  	}
 
  	uniform_fbo_texture = glGetUniformLocation(program_postproc, "fbo_texture");
  	if (uniform_fbo_texture == -1) {
  	  fprintf(stderr, "Could not bind uniform fbo_texture\n");
  	  return 0;
  	}

	return 1;
}

int setupParticleSystem(float *h_pos, int numBodies, int stride)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*numBodies*stride, NULL, GL_STATIC_DRAW);
	
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(float)*numBodies*stride, h_pos );
	
	//Assign the 'in vec4 vPosition' from our shader to the vec3 for each point in our array
	loc = glGetAttribLocation(program, "vPosition");
	glVertexAttribPointer(loc, stride, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(loc);

	//Set the uniform variable in the fragment shader to the color white
	//This variable will change later depending on the keyboard.
	GLfloat defaultColorWhite[4] = {1.0,1.0,1.0,1.0}; 
	GLint fcolorLoc = glGetUniformLocation(program, "fcolor");
	glUniform4fv(fcolorLoc, 1, defaultColorWhite );
	return 1;
}

//Sets up the default matrices used for all the matrix math.
void initMatrices()
{
	// Set up default matrices for the world/view/projection
	world = Translate(0,0,0);
	view = LookAt( eyePosition, vec4(0,0,0,0), vec4(0,1,0,0) );
	proj = Perspective(60, aspectRat, 1,100);
		
	//Combine world and view into one so only 1 mat needs to be passed to the vshader
	mat4 WorldView = view * world;

	//Pass WorldView and proj to the vshader
	GLint worldViewLoc = glGetUniformLocation(program, "WorldView");
	glUniformMatrix4fv(worldViewLoc, 1, false, WorldView);
	GLint projLoc = glGetUniformLocation(program, "Proj");
	glUniformMatrix4fv(projLoc, 1, false, proj);
}

//When the machine is idling, rotate the system.
void onIdle() 
{
	
}

//Makes the screen draw at 30fps
void callbackTimer(int)
{
	glutTimerFunc(1000/30, callbackTimer, 0);
	glutPostRedisplay();

}

//draws a current particle system with a specific offset in a specific color.
void drawParticles(GLfloat x, GLfloat y,GLfloat z, int numBodies, vec4 color)
{
	GLint fcolorLoc = glGetUniformLocation(program, "fcolor");
	glUniform4fv(fcolorLoc, 1, color );

	world = Translate(x,y,z);
	mat4 WorldView = view * world;
	GLint worldViewLoc = glGetUniformLocation(program, "WorldView");
	glUniformMatrix4fv(worldViewLoc, 1, false, WorldView);
	glDrawArrays(GL_POINTS, 0, numBodies);
}

//code to redraw the particles based on data from simulation
void reDrawParticles(float* h_pos, int numBodies, int stride){

	//Load particle data contained in h_pos into the GPU
	setupParticleSystem(particles, 1024, 4);
	//set the global color in the shader to Green (RGBA = 0,1.0,0,1.0)
	GLint fcolorLoc = glGetUniformLocation(program, "fcolor");
	glUniform4fv(fcolorLoc, 1, vec4(0.0,1.0,0.0,1.0) );

	//Draw Particles to the screen
	glDrawArrays(GL_POINTS, 0, numBodies);
}

void sendDepthData()
{
	int temp = 1;
	int i;
	char* depthbytes = (char*) depthResult;
	char tempMsg[4];
	for(i = 0; i < screen_width*screen_height*sizeof(GLfloat) && temp > 0; )
	{
		
		int sendAmount = sizeof(GLfloat) * 8192;
		if(sendAmount + i > screen_width*screen_height*sizeof(GLfloat))
			sendAmount = screen_width*screen_height*sizeof(GLfloat) - i;
		temp = sendto(sendToSocket, &depthbytes[i], sendAmount , 0, (struct sockaddr *) &sendToServaddr, sizeof(sendToServaddr));
		//printf(" Current: %d, this round : %d\n", i , temp);
		i += temp;
	
		
		printf(" Current: %d, this round : %d\n", i , temp);

		recvfrom(feedbackSocket, tempMsg, 2 , 0, (struct sockaddr *) &fbaddr, (socklen_t*) &fbsocklen);
		tempMsg[3] = '\0';
		printf("Got Message: %s \n", tempMsg);
		

	}
	//printf("Sent %d, status %d, length %ld\n", i, temp, (screen_width*screen_height*sizeof(GLfloat)));
	//printf("Error in socket - %d\n",sock_errno());
}

void checkSocketForParticlesandDraw()
{
	//The following 4 lines sets up the timer that determines how long the visualizer should wait for new particles before displaying the old set
	tv.tv_sec = 0; 
	tv.tv_usec = 150; //Wait 150 microsecs
	FD_ZERO(&fds);
	FD_SET(list_s,&fds);

	int timeout = 0;
	int select_result = select(sizeof(fds)*8,&fds,NULL,NULL,&tv);
	if(select_result > 0)	// only execute the following code if something is new particles are waiting in the socket.
	{
		//printf("Got stuff\n");
		//Stores new info in the "particles" buffer
		int n = recvfrom(list_s, particles, 4096, 0, (struct sockaddr *)&servaddr_other, (socklen_t*)&socklen);
		if (n !=-1)
		{
			reDrawParticles(particles, n / 4, 4);
		}
	}
	else if(select_result < 0)
	{
		error((char*)"Error with select function occurred\n");
	}

	else // Timeout occurred: this means that the simulation hasnt sent any new info over, so we should draw what we already have.
	{
		//This time we're going to draw the particles Red (RGBA = 1-0-0-1)
		setupParticleSystem(particles, 1024, 4);
  		drawParticles(0,0,0,1024, vec4(1.0,0.0,0.0,1.0));
	}
	glutSwapBuffers();
}

void captureDepthData()
{
	glReadPixels(0,0,screen_width,screen_height,GL_DEPTH_COMPONENT, GL_FLOAT, depthResult);
}

void setupOffscreenTarget()
{
	//Switch render targets to the offscreen renderer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	
	//Clear the screen to Black (RGBA = 0-0-0-1)	
	glClearColor(0.0, 0.0, 0.0, 1.0);
	//Clear both the color and the depth buffers
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	//Make the size of the particles displayed to 3.0 (1.0 is about 1 pixel per particle)
	glPointSize(3.0);

	glUseProgram(program);
}

void setupGLUTTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClearColor(0.0, 0.0, 0.0, 1.0);
  	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
 
  	glUseProgram(program_postproc);
}

void drawCapturedRenderTarget()
{
	glBindTexture(GL_TEXTURE_2D, fbo_texture);
  	glUniform1i(uniform_fbo_texture, 0);
  	glEnableVertexAttribArray(attribute_v_coord_postproc);
 	
  	glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
  	glVertexAttribPointer(
   		attribute_v_coord_postproc,  // attribute
   		2,                  // number of elements per vertex, here (x,y)
   		GL_FLOAT,           // the type of each element
   		GL_FALSE,           // take our values as-is
   		0,                  // no extra data between each position
   		0                   // offset of first element
  		);

  	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  	glDisableVertexAttribArray(attribute_v_coord_postproc);
	//Actual screen refresh occurs here
	glutSwapBuffers();
}

//Function that does all the actual displaying.
void onDisplay()
{	

	setupOffscreenTarget();
	checkSocketForParticlesandDraw();

	captureDepthData();
	
	//TODO
	//Sending of captured render is currently done MANUALLY, with the "p" key while the window is in focus.
	//This is to prevent a flood of data on the network
	//Some kind of synchronization / staggering of the simulation sending particle data and the LocalVisualizer sending depth buffers needs to be created.

	//sendDepthData();
	
	setupGLUTTarget();
	
	drawCapturedRenderTarget();
	
}

//Callback function for window resizing
void onReshape(int width, int height) {
  //Dont allow resizing while buffers are a static size
  //screen_width = width;
  //screen_height = height;
  glViewport(0, 0, screen_width, screen_height);
}

//Resets position of camera back to default.
void resetView()
{
	eyePosition = vec4(0,0,50,0);
	eyeDirection = vec4(0,0,-1,0);
	perpEyeDirection = vec4(1,0,0,0);
	upVec = vec4(0,1,0,0);

	xAngle = 270;
	view = LookAt( eyePosition, eyePosition + (500*eyeDirection), upVec );
}

void moveCam(float side, float forward)
{
	eyePosition += forward * eyeDirection;
	eyePosition += side * perpEyeDirection;
	
	view = LookAt( eyePosition, eyePosition + eyeDirection, upVec );
}

void turnCam(float side, float up)
{
	xAngle += side; 
	eyePosition+= vec4(0,up,0,0);
	
	eyeDirection = vec4(cos(xAngle*PI/180.0), 0, sin(xAngle*PI/180.0), 0 );

	//std::cout << perpEyeDirection << std::endl;
	
	perpEyeDirection = vec4(cos( (xAngle+90)*PI/180.0), 0, sin( (xAngle+90) *PI/180.0), 0 );

	//std::cout << perpEyeDirection << std::endl;

	view = LookAt( eyePosition, eyePosition + (10*eyeDirection), upVec );
}

void callbackKeyboard(unsigned char key, int x, int y)
{
	if(key == 'r' || key == 'R') 
		resetView();
	if(key == 'w' || key == 'W') 
		moveCam(0,0.25);
	if(key == 'a' || key == 'A') 
		moveCam(-0.25,0);
	if(key == 's' || key == 'S') 
		moveCam(0,-0.25);
	if(key == 'd' || key == 'D') 
		moveCam(0.25,0);
	if(key == 'p' || key == 'P')
	{
		//Counts number of pixels that represent a depth value, and sends them to the next node for processing.
		int count = 0;
		for(int x = 0; x < screen_width*screen_height; x++)
		{
			if(depthResult[x] != 1.0)
				count++;
		}
		printf("Count = %d. Sending to Combiner.\n", count);
		sendDepthData();
		
	}
}

void callbackSpecialKeys(int key, int x, int y)
{
	if(key == GLUT_KEY_UP) 
		turnCam(0,0.25);
	if(key == GLUT_KEY_DOWN) 
		turnCam(0,-0.25);
	if(key == GLUT_KEY_LEFT) 
		turnCam(-1,0);
	if(key == GLUT_KEY_RIGHT) 
		turnCam(1,0);
}

void freeResources()
{
	glDeleteRenderbuffers(1, &rbo_depth);
  	glDeleteTextures(1, &fbo_texture);
  	glDeleteFramebuffers(1, &fbo);
}

void initCallbacks()
{
	glutDisplayFunc(onDisplay);
	glutReshapeFunc(onReshape);
	glutIdleFunc(onIdle);
	glutTimerFunc(1000/30, callbackTimer, 0);
	glutKeyboardFunc(callbackKeyboard);
	glutSpecialFunc(callbackSpecialKeys);
}

int main(int argc, char* argv[]) 
{
    /*  Get port number from the command line, and
        set to default port if no arguments were supplied  */
	socklen = sizeof(servaddr_other);
	fbsocklen = sizeof(fbaddr);

	//TODO
	//Proper selection of ports, IP addresses for network setup.
	//Currently needs to be set manually in the source, other than the port particles are received on.
	
    if ( argc == 2 ) {
	port = strtol(argv[1], &endptr, 0);
	toPort = 2003;
	fbPort = 2005;
	if ( *endptr ) {
	    fprintf(stderr, "ECHOSERV: Invalid port number.\n");
	    exit(EXIT_FAILURE);
	}
    }
    else if ( argc < 2) {
	port = ECHO_PORT;
	toPort = 2003;
	fbPort = 2005;
    }
    else {
	fprintf(stderr, "ECHOSERV: Invalid arguments.\n");
	exit(EXIT_FAILURE);
    }

	//Socket Setup
    	if ( (list_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
		exit(EXIT_FAILURE);
    	}
	
	if ( (sendToSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOCLNT: Error creating sendTo socket.\n");
		exit(EXIT_FAILURE);
    	}
	
	if ( (feedbackSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOCLNT: Error creating sendToFeedback socket.\n");
		exit(EXIT_FAILURE);
    	}

    	/*  Set all bytes in socket address structure to
        zero, and fill in the relevant data members   */

    	memset(&servaddr, 0, sizeof(servaddr));
    	servaddr.sin_family      = AF_INET;
    	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    	servaddr.sin_port        = htons(port);

	memset(&fbaddr, 0, sizeof(fbaddr));
    	fbaddr.sin_family      = AF_INET;
    	fbaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    	fbaddr.sin_port        = htons(fbPort);

	memset(&sendToServaddr, 0, sizeof(sendToServaddr));
    	sendToServaddr.sin_family      = AF_INET;
    	sendToServaddr.sin_port        = htons(toPort);

	//TODO
	//Allow for user to pick the ip address we will send renders to.
	if(inet_aton("127.0.0.1", &sendToServaddr.sin_addr) <=0)
	{
		printf("Invalid remote IP address for visualization target.\n");
		exit(EXIT_FAILURE);
    	}

    	if ( bind(list_s, (struct sockaddr *) &servaddr, (socklen_t)socklen) == -1 ) {
		fprintf(stderr, "ECHOSERV: Error calling bind()\n");
		exit(EXIT_FAILURE);
    	}

	if ( bind(feedbackSocket, (struct sockaddr *) &fbaddr, (socklen_t)socklen) == -1 ) {
		fprintf(stderr, "ECHOSERV: Error calling bind()\n");
		exit(EXIT_FAILURE);
    	}


	//Setup the glut window
  	glutInit(&argc, argv);
  	glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
  	glutInitWindowSize(screen_width, screen_height);
  	glutCreateWindow("Local N-Body Client");
  	GLenum glew_status = glewInit();
  	if (glew_status != GLEW_OK) 
	{
    		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
    		return 1;
 	}

  	if (!GLEW_VERSION_2_0) 
	{
    		fprintf(stderr, "Error: your graphics card does not support OpenGL 2.0\n");
    		return 1;
  	}
  	
	//Setup buffers for dummy particle system, if it works we're good to go
	if (initParticleSystem()) 
	{
		setupParticleSystem(particles, 1024 , 4);
		//Attach glut callback functions to their events
    		initCallbacks();

		//Setup basic global matrices
    		initMatrices();
    
		//Enable blend for alpha stuff
    		glEnable(GL_BLEND);
    		//Enable depth testing for drawing
    		glEnable( GL_DEPTH_TEST );
		
    		//This was here for me to test wireframe mode earlier (isnt right now)
   		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    		//glDepthFunc(GL_LESS);
    		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//done with setup, begin main program run   
		printf("Welcome to the N-body Visualizer client.\n Controls: WASD to move Forward, Left, Backward, Right.\n Arrow keys to turn.\n R to reset view to starting position.\nOpenGL version: %s\n", glGetString(GL_VERSION));
		glutMainLoop();
  	}

  	return 0;
}
