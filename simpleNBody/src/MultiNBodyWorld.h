/*
 * multiNBodyWorld.h
 *
 *  Created on: Oct 9, 2012
 *      Author: martin
 */

#ifndef MULTINBODYWORLD_H_
#define MULTINBODYWORLD_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "World.h"
#include <cstdlib>

class NBodyParams;

class MultiNBodyWorld: public World {
public:
	MultiNBodyWorld();
	virtual ~MultiNBodyWorld();

	virtual void init(MPI_Comm comm_main){
		World::init(comm_main);
		init();
	}

	virtual void init();
	virtual void loadInput(int argc, char** argv);
	virtual void run(){while(time<tlim){step(dt);time+=dt;iter++;}}

	virtual void step(float indt);
	void onDisplay();
	void onIdle();
	int init_resources();
	void onReshape(int width, int height);
	void free_resources();

	int screen_width, screen_height;
	void setPortIP(int port, char* ip){
		PORT = port;
		printf("Port number: %d\n\n",PORT);
		printf(IP);
		IP = ip;
	}
	void sendHPos(float hpos[], int size);
private:

	

	int numBodies;

	NBodyParams* demoParams;	// Set of Initial Condition Parameters from SDK Demo
	int nDemoParams;			// Number of Demo Parameters to Select From
	int nDoms;					// Number of Domains to Initialize from Command Line
	bool useGpu;				// Use of GPU flag from Command Line
	int PORT;
	char* IP;

};

#endif /* MULTINBODYWORLD_H_ */
