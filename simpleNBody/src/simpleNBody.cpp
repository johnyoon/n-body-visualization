/*
 * simpleNBody.cpp
 *
 *  Created on: Oct 1, 2012
 *      Author: martin
 */

#include <stdio.h>
#include "MultiNBodyWorld.h"
#include <mpi.h>
#include <stdlib.h>


MultiNBodyWorld* theWorld;

int main(int argc, char** argv)
{	
	// Initialize MPI
	int rank, numprocs;
	MPI_Comm comm_main;
	MPI_Init(&argc,&argv);
	MPI_Comm_dup(MPI_COMM_WORLD,&comm_main);
	MPI_Comm_rank(comm_main,&rank);

	// Startup
	if(rank==0)	
	{	
		printf("-------------CS130 - Simple NBbody 1.0-------------\n");
		printf("Usage: .../simpleNBody [IP Address of local visualizer] [port number]\n")
	}

	// Initialization Block - Must Destroy theWorld before MPI_Finalize
	theWorld=new MultiNBodyWorld();

	theWorld->loadInput(argc,argv);
	theWorld->init(comm_main);
	if(argc == 3){
		char *endptr;
		theWorld->setPortIP(strtol(argv[2], &endptr, 0), argv[1]);
		printf(argv[1]);
		printf("\n");
		printf(argv[2]);
		
	}else{
		theWorld->setPortIP(2002, "127.0.0.1");
	}

	MPI_Barrier(comm_main);

	// Run Block
	if(rank==0)printf("Running NBody Systems\n");
	theWorld->run(); //Use as Glut's OnDisplay-esque function?	
			 //I think this will work , but OpenGL variables will need to be passed in somehow
				//TESTING


	int nDoms = theWorld->getNDom();
	Domain* workingDomain = theWorld->getDomain(nDoms);
	int np = workingDomain->returnNP();
	float* sentPositionArray=new float[4*np*numprocs];
	
	/*if(rank!=0){
		MPI_Send(workingDomain->returnPos(), 4*workingDomain->returnNP(), MPI_FLOAT, 0, 1, comm_main);		
	}else{
		MPI_Recv(sentPostionArray, 4*np*numprocs, 
	}*/

	MPI_Gather(workingDomain->returnPos(), 4*workingDomain->returnNP(), MPI_FLOAT, sentPositionArray,4*np*numprocs, MPI_FLOAT, 0, comm_main);
	// Cleanup
	MPI_Barrier(comm_main);
	delete theWorld;

	MPI_Finalize();
	return (EXIT_SUCCESS);
}











