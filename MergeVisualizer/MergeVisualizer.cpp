/**
 * From the OpenGL Programming wikibook: http://en.wikibooks.org/wiki/OpenGL_Programming
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glew.h>

#include <GL/glut.h>
#include <GL/glext.h>


/* GLM */
// #define GLM_MESSAGES
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
#include "../CommonGraphics/shader_utils.h"
//Angel.h provides matrix calculations, vec objects
#include "../CommonGraphics/Angel.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include <cstring> // Memset
#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */
#include <time.h>
#include <errno.h>

#define ECHO_PORT          (2004)
#define MAX_LINE           (100000)
#define LISTENQ            (1024) 


/* Globals for render-to-texture */
int screen_width=640, screen_height=480;

//The following two lines are for the program/shaders that will combine multiple separate renders together
GLuint fbo, rbo_depth;
GLuint vbo_fbo_vertices;
GLuint fbo_texture;
GLuint program, attribute_v_coord, uniform_fbo_texture;
GLuint uniform_texture1, uniform_texture2;

GLuint uniform_texture1_combine, uniform_texture2_combine;
GLuint program_combine, v_coord_combine, uniform_fbo_combine;

GLuint texture1, texture2;

//Following buffers should eventually be dynamically allocated rather than defined. Same for screen width/height too.
GLfloat textureLeftBuffer[640*480];
GLfloat textureRightBuffer[640*480];
GLfloat outputRenderBuffer[640*680];


//Only used for debug feedback-loop demo
int debugcheck  = 0;

int       	receivingSocketLeft;         /*  listening socket: receives depth render 1 */
int       	receivingSocketRight;        /*  listening socket: receives depth render 2 */
int		feedbackSocketLeft;	//Used to mediate messages between 1st sender
int		feedbackSocketRight;	//Used to mediate messages between 2nd sender

int 		sendingSocket;		// Socket that current visualizers depth data is sent out on.
int 		sendingFeedbackSocket;   // Receives feed back from destination to control 

short int 	portLeft;                  	/*  port number receiver1              */
short int 	portRight;                  	/*  port number receiver2              */
short int 	feedbackPortLeft;		//port number feedback 1
short int 	feedbackPortRight;		//port number feedback 2

short int 	sendPort;
short int 	sendfbPort;

struct    	sockaddr_in servaddrLeft, 
		servaddrRight,
		fbaddrLeft,
		fbaddrRight,
		sendingaddr,
		sendingfbaddr,
		servaddr_other;  	/*  socket address structure  */
char     	*endptr;                /*  for strtol()              */
int 		socklenLeft;
int 		socklenRight;
int		sendingfbsocklen;


void error(char *msg)
{
    perror(msg);
    exit(1);
}


//creates shaders for particle system, the program to run, and sets up buffers with particle data
//This will eventually need to be split up further, so that the shader creation occurs only once
//But the bufferes can be reset for new values.
int initDisplayShader()
{
	GLint link_ok = GL_FALSE;
	GLint validate_ok = GL_FALSE;

  	GLuint vs, fs;
	if ((vs = create_shader("combinev.glsl", GL_VERTEX_SHADER))   == 0) return 0;
	if ((fs = create_shader("displayf.glsl", GL_FRAGMENT_SHADER)) == 0) return 0;

	program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
	if (!link_ok) 
	{
		fprintf(stderr, "glLinkProgram:");
		print_log(program);
		return 0;
	}

	glUseProgram(program);
	return 1;
}

int initCombineShader()
{
	GLint link_ok = GL_FALSE;
	GLint validate_ok = GL_FALSE;

  	GLuint vs, fs;
	if ((vs = create_shader("combinev.glsl", GL_VERTEX_SHADER))   == 0) return 0;
	if ((fs = create_shader("combinef.glsl", GL_FRAGMENT_SHADER)) == 0) return 0;

	program_combine = glCreateProgram();
	glAttachShader(program_combine, vs);
	glAttachShader(program_combine, fs);
	glLinkProgram(program_combine);
	glGetProgramiv(program_combine, GL_LINK_STATUS, &link_ok);
	if (!link_ok) 
	{
		fprintf(stderr, "glLinkProgram:");
		print_log(program_combine);
		return 0;
	}

	glUseProgram(program_combine);
	return 1;
}

int initTexturesandRenderBuffers()
{	
	//Set up framebuffer for offscreen rendering
	/* Create back-buffer, used for post-processing */
 
 	/* Texture for offscreen render target */
  	glActiveTexture(GL_TEXTURE0);
  	glGenTextures(1, &fbo_texture);
  	glBindTexture(GL_TEXTURE_2D, fbo_texture);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, screen_width, screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  	glBindTexture(GL_TEXTURE_2D, 0);
 
  	/* Depth buffer */
  	glGenRenderbuffers(1, &rbo_depth);
  	glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth);
  	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, screen_width, screen_height);
  	glBindRenderbuffer(GL_RENDERBUFFER, 0);
 
  	/* Framebuffer to link everything together */
  	glGenFramebuffers(1, &fbo);
  	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0);
  	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo_depth);
  	GLenum status;
  	if ((status = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE) {
    		fprintf(stderr, "glCheckFramebufferStatus: error %d", status);
    		return 0;
  	}
  	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/* Texture1 Left Setup */
  	glActiveTexture(GL_TEXTURE0);
  	glGenTextures(1, &texture1);
  	glBindTexture(GL_TEXTURE_2D, texture1);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, screen_width, screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  	glBindTexture(GL_TEXTURE_2D, 0);

	/* Texture2 Right setup*/
  	glActiveTexture(GL_TEXTURE1);
  	glGenTextures(1, &texture2);
  	glBindTexture(GL_TEXTURE_2D, texture2);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, screen_width, screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  	glBindTexture(GL_TEXTURE_2D, 0);


	return 1;
}

int initShaderResources()
{
	//Create vertext array for second mode (combining screencaptures)
	GLfloat fbo_vertices[] = {
    				-1, -1,
    				 1, -1,
    				-1,  1,
    				 1,  1,
  				};

  	glGenBuffers(1, &vbo_fbo_vertices);
  	glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
  	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);
  	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	//For display shader
	glUseProgram(program);
  	attribute_v_coord = glGetAttribLocation(program, "v_coord");
  	if (attribute_v_coord == -1) {
  	  fprintf(stderr, "Could not bind attribute v_coord\n");
  	  return 0;
  	}
 
  	uniform_texture1 = glGetUniformLocation(program, "texture1");
	uniform_texture2 = glGetUniformLocation(program, "texture2");
  	  	
	if (uniform_texture1 == -1) {
  	  fprintf(stderr, "Could not bind uniform texture 1\n");
  	  return 0;
  	}

	
	if (uniform_texture2 == -1){
		fprintf(stderr, "Could not bind uniform texture 2\n");
  	  return 0;

	}

	glUniform1i(uniform_texture1, 0);
	glUniform1i(uniform_texture2, 1);

	glUseProgram(program_combine);

	//For real combine shader for renders that get passed to another node
	v_coord_combine = glGetAttribLocation(program_combine, "v_coord");
  	if (v_coord_combine == -1) {
  	  fprintf(stderr, "Could not bind attribute v_coord_combine\n");
  	  return 0;
  	}
 
  	uniform_texture1_combine = glGetUniformLocation(program_combine, "texture1");
	uniform_texture2_combine = glGetUniformLocation(program_combine, "texture2");
  	  	
	if (uniform_texture1_combine == -1) {
  	  fprintf(stderr, "Could not bind uniform texture 1_combine\n");
  	  return 0;
  	}

	
	if (uniform_texture2_combine == -1){
		fprintf(stderr, "Could not bind uniform texture 2_combine\n");
  	  return 0;

	}

	glUniform1i(uniform_texture1_combine, 0);
	glUniform1i(uniform_texture2_combine, 1);

	return 1;
}

void onIdle() 
{
	
}

//Makes the screen draw at 30fps
void callbackTimer(int)
{
	glutTimerFunc(1000/30, callbackTimer, 0);
	glutPostRedisplay();

}

void receiveFromSocketLeft()
{
	int temp = 1;
	int i;
	char* depthbytes = (char*) textureLeftBuffer;
	printf("Waiting for render 1\n");
	for(i = 0; i < screen_width*screen_height*sizeof(GLfloat) && temp > 0; )
	{
		int recvAmount = sizeof(GLfloat) * 8192;
		if(recvAmount + i > screen_width*screen_height*sizeof(GLfloat))
			recvAmount = screen_width*screen_height*sizeof(GLfloat) - i;
		temp = recvfrom(receivingSocketLeft, &depthbytes[i], recvAmount , 0, (struct sockaddr *) &servaddrLeft, (socklen_t*) &socklenLeft);
		i += temp;

		int sendtemp = sendto(feedbackSocketLeft, "OK", 2 , 0, (struct sockaddr *) &fbaddrLeft, sizeof(fbaddrLeft));
		
	}

	printf(" Recieved render 1;\n");
}

void getDepthDataLeft()
{

	receiveFromSocketLeft();

  	glBindTexture(GL_TEXTURE_2D, texture1);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, screen_width, screen_height, 0, GL_RED, GL_FLOAT, textureLeftBuffer);
}

void receiveFromSocketRight()
{
	int temp = 1;
	int i;
	char* depthbytes = (char*) textureRightBuffer;
	
	printf("Waiting for render 2\n");

	//Current actual receiving implementation.Comment out "debugcheck" if statement below when using this.
	/*
	for(i = 0; i < screen_width*screen_height*sizeof(GLfloat) && temp > 0; )
	{
		//printf("inside loop i:%d\n",i);
		int recvAmount = sizeof(GLfloat) * 8192;
		if(recvAmount + i > screen_width*screen_height*sizeof(GLfloat))
			recvAmount = screen_width*screen_height*sizeof(GLfloat) - i;
		temp = recvfrom(receivingSocketRight, &depthbytes[i], recvAmount , 0, (struct sockaddr *) &servaddrRight, (socklen_t*) &socklenRight);
		i += temp;

		int sendtemp = sendto(feedbackSocketRight, "OK", 2 , 0, (struct sockaddr *) &fbaddrRight, sizeof(fbaddrRight));
		
	}*/

	//Following section was debug code for easier testing on a single machine
	// comment out above for-loop if you wish to use this.
	// also see the "onDisplay" function for another section to uncomment
	
	if(debugcheck == 0)
	{
		for(int x = 0; x < screen_width*screen_height; x++)
		{
		textureRightBuffer[x] = 1.0; //textureLeftBuffer[screen_width*screen_height - 1 - x];
		}
		debugcheck = 1;
	}

	printf(" Recieved render 2;\n");
}

void getDepthDataRight()
{
	receiveFromSocketRight();

  	glBindTexture(GL_TEXTURE_2D, texture2);
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, screen_width, screen_height, 0, GL_RED, GL_FLOAT, textureRightBuffer);
	
}


void sendOutputRender()
{
	int temp = 1;
	int i;
	char* depthbytes = (char*) outputRenderBuffer;
	char tempMsg[4];
	for(i = 0; i < screen_width*screen_height*sizeof(GLfloat) && temp > 0; )
	{
		
		int sendAmount = sizeof(GLfloat) * 8192;
		if(sendAmount + i > screen_width*screen_height*sizeof(GLfloat))
			sendAmount = screen_width*screen_height*sizeof(GLfloat) - i;
		temp = sendto(sendingSocket, &depthbytes[i], sendAmount , 0, (struct sockaddr *) &sendingaddr, sizeof(sendingaddr));
		//printf(" Current: %d, this round : %d\n", i , temp);
		i += temp;
	
		
		printf(" Current: %d, this round : %d\n", i , temp);

		recvfrom(sendingFeedbackSocket, tempMsg, 2 , 0, (struct sockaddr *) &sendingfbaddr, (socklen_t*) &sendingfbsocklen);
		tempMsg[3] = '\0';
		printf("Got Message: %s \n", tempMsg);
		

	}
}

void setupOffscreenTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo); 
	glClearColor(0.0, 0.0, 0.0, 1.0);	
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(program_combine);
	glEnableVertexAttribArray(v_coord_combine);
 	
  	glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
  	glVertexAttribPointer(
   		attribute_v_coord,  // attribute
   		2,                  // number of elements per vertex, here (x,y)
   		GL_FLOAT,           // the type of each element
   		GL_FALSE,           // take our values as-is
   		0,                  // no extra data between each position
   		0                   // offset of first element
  		);
}

void setupGLUTTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(program);
	glEnableVertexAttribArray(attribute_v_coord);
 	
  	glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
  	glVertexAttribPointer(
   		attribute_v_coord,  // attribute
   		2,                  // number of elements per vertex, here (x,y)
   		GL_FLOAT,           // the type of each element
   		GL_FALSE,           // take our values as-is
   		0,                  // no extra data between each position
   		0                   // offset of first element
  		);
}

void drawWithCurrentProgram()
{
	GLint anint;
	glActiveTexture(GL_TEXTURE0);
  	glBindTexture(GL_TEXTURE_2D, texture1);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);
	printf("Texture0: %d\n", anint);
	
	glActiveTexture(GL_TEXTURE1);	
  	glBindTexture(GL_TEXTURE_2D, texture2);
	glGetIntegerv(GL_ACTIVE_TEXTURE, &anint);
	printf("Texture1: %d\n", anint);
	
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	

	glutSwapBuffers();
}

void captureCombinedRender()
{
	//Here we are only capturing the RED component of the screen.
	//This is because the offscreen buffer was setup as an R32F frame buffer, where only the red channel is used and each pixel gets a full 32bit float for this channel.
	//My combinef.glsl shader writes the correct DEPTH information into the color channel, so everything comes out the same in the end.
	glReadPixels(0,0,screen_width, screen_height,GL_RED, GL_FLOAT, outputRenderBuffer);
}

//Function that does all the actual displaying.
void onDisplay()
{
	setupOffscreenTarget();
	
	getDepthDataLeft();
	getDepthDataRight();
	
	drawWithCurrentProgram();
	
	//Used for debugging checks
	//comment the functionbelo, while uncommenting "captureCombinedRender"below, in addition to uncommenting code in getDepthDataRight for non-debug mode.
	glReadPixels(0,0,screen_width, screen_height,GL_RED, GL_FLOAT, textureRightBuffer);
	//captureCombinedRender();

	setupGLUTTarget();
	drawWithCurrentProgram();

	
	//Sending sockets not implemented yet, no way to test network conditions.
	//sendOutputRender();

	//TODO
	//Complete sending info, uncomment above function.
}


//Callback function for window resizing

void onReshape(int width, int height) {
  //Dont allow resizing while buffers are set for specific resolution
  //screen_width = width;
  //screen_height = height;
  glViewport(0, 0, screen_width, screen_height);
}

//TODO:
//Send visualization navigation instructions to (all) clients.
// The LocalVisualizer Has these functions all supported, so all thats needed is, when a key is pressed at the final viewer, for that key press to be sent all the way down to all the local visualizers.
// This can be any sort of custom message (through a socket, pipe, whatever) that just calls the functions listed in this method in the LocalVisualizer code.
void callbackKeyboard(unsigned char key, int x, int y)
{
	if(key == 'r' || key == 'R') 
		printf("Key R currently not supported\n.");
	if(key == 'w' || key == 'W') 
		printf("Key W currently not supported\n.");
	if(key == 'a' || key == 'A') 
		printf("Key A currently not supported\n.");
	if(key == 's' || key == 'S') 
		printf("Key S currently not supported\n.");
	if(key == 'd' || key == 'D') 
		printf("Key D currently not supported\n.");
	
}

//TODO: See above, same goes for this function here
void callbackSpecialKeys(int key, int x, int y)
{
	if(key == GLUT_KEY_UP) 
		printf("Key UP currently not supported\n.");
	if(key == GLUT_KEY_DOWN) 
		printf("Key DOWN currently not supported\n.");
	if(key == GLUT_KEY_LEFT) 
		printf("Key LEFT currently not supported\n.");
	if(key == GLUT_KEY_RIGHT) 
		printf("Key RIGHT currently not supported\n.");
}

void freeResources()
{
	glDeleteRenderbuffers(1, &rbo_depth);
  	glDeleteTextures(1, &fbo_texture);
	glDeleteTextures(1, &texture1);
	glDeleteTextures(1, &texture2);
  	glDeleteFramebuffers(1, &fbo);
}

void initCallbacks()
{
	glutDisplayFunc(onDisplay);
	glutReshapeFunc(onReshape);
	glutIdleFunc(onIdle);
	glutTimerFunc(1000/30, callbackTimer, 0);
	glutKeyboardFunc(callbackKeyboard);
	glutSpecialFunc(callbackSpecialKeys);
}

void initSockets()
{
	socklenLeft = sizeof(servaddrLeft);
	socklenRight = sizeof(servaddrRight);
	sendingfbsocklen = sizeof(sendingfbaddr);

	//Create all 6 sockets.
	if ( (receivingSocketLeft = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
		exit(EXIT_FAILURE);
    	}

    	if ( (feedbackSocketLeft = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOCLNT: Error creating sendTo socket.\n");
		exit(EXIT_FAILURE);
    	}

    	if ( (receivingSocketRight = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
		exit(EXIT_FAILURE);
    	}

    	if ( (feedbackSocketRight = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOCLNT: Error creating sendTo socket.\n");
		exit(EXIT_FAILURE);
    	}

	if ( (sendingSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOSERV: Error creating listening socket.\n");
		exit(EXIT_FAILURE);
    	}

    	if ( (sendingFeedbackSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		fprintf(stderr, "ECHOCLNT: Error creating sendTo socket.\n");
		exit(EXIT_FAILURE);
    	}

	//Assign port numbers
	portLeft = 2003;
	portRight = 2004;
	feedbackPortLeft = 2005;
	feedbackPortRight = 2006;

	sendPort = 2007;
	sendfbPort = 2008;

	//Setup left receiving socket and its feedback socket
	memset(&servaddrLeft, 0, sizeof(servaddrLeft));
    	servaddrLeft.sin_family      = AF_INET;
    	servaddrLeft.sin_addr.s_addr = htonl(INADDR_ANY);
    	servaddrLeft.sin_port        = htons(portLeft);

    	memset(&fbaddrLeft, 0, sizeof(fbaddrLeft));
    	fbaddrLeft.sin_family      = AF_INET;
    	fbaddrLeft.sin_port        = htons(feedbackPortLeft);

    	if(inet_aton("127.0.0.1", &fbaddrLeft.sin_addr) <=0)
    	{
		printf("Invalid remote IP address for visualization target.\n");
		exit(EXIT_FAILURE);
    	}

	//setup right recieving socket and its feedback socket
	memset(&servaddrRight, 0, sizeof(servaddrRight));
    	servaddrRight.sin_family      = AF_INET;
    	servaddrRight.sin_addr.s_addr = htonl(INADDR_ANY);
    	servaddrRight.sin_port        = htons(portRight);

    	memset(&fbaddrRight, 0, sizeof(fbaddrRight));
    	fbaddrRight.sin_family      = AF_INET;
    	fbaddrRight.sin_port        = htons(feedbackPortRight);

    	if(inet_aton("127.0.0.1", &fbaddrRight.sin_addr) <=0)
    	{
		printf("Invalid remote IP address for visualization target RIGHT.\n");
		exit(EXIT_FAILURE);
    	}
		
	//TODO
	//Setup sending socket addr

	//TODO
	//Setup Feedback sending socket addr

	//TODO
	//inet_aton for feedback sending addr
	
	//Bind the sockets that receive data.
	if ( bind(receivingSocketLeft, (struct sockaddr *) &servaddrLeft, (socklen_t)socklenLeft) == -1 ) {
		fprintf(stderr, "ECHOSERV: Error calling bind() on left receiver\n");
		exit(EXIT_FAILURE);
    	}

    	if ( bind(receivingSocketRight, (struct sockaddr *) &servaddrRight, (socklen_t)socklenRight) == -1 ) {
		fprintf(stderr, "ECHOSERV: Error calling bind() on right receiver\n");
		exit(EXIT_FAILURE);
    	}

	//TODO
	//Bind sending feedback socket

}

int main(int argc, char* argv[]) 
{	
    	initSockets();

	//Setup the glut window
  	glutInit(&argc, argv);
  	glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
  	glutInitWindowSize(screen_width, screen_height);
  	glutCreateWindow("Merge N-Body Client");
  	GLenum glew_status = glewInit();
  	if (glew_status != GLEW_OK) 
	{
    		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
    		return 1;
 	}

  	if (!GLEW_VERSION_2_0) 
	{
    		fprintf(stderr, "Error: your graphic card does not support OpenGL 2.0\n");
    		return 1;
  	}
	//First one will fail if there's some kind of opengl Issue
	//Should probably do sanity checks on the rest of the inits
	if (initCombineShader()) 
	{
		
		//TODO
		//Sanity checks on the rest of the inits
		initDisplayShader();
	
		initTexturesandRenderBuffers();
		initShaderResources();
		initCallbacks();
    
		//Enable blend for alpha stuff
    		glEnable(GL_BLEND);
    		//Enable depth testing for drawing
    		glEnable( GL_DEPTH_TEST );
   		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    		//glDepthFunc(GL_LESS);
    		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//done with setup, begin main program run   
		printf("Welcome to the N-body Visualizer Merge client.\nOpenGL version: %s\n", glGetString(GL_VERSION));
		glutMainLoop();
  	}

  	return 0;
}
