uniform sampler2D texture1;
uniform sampler2D texture2;
varying vec2 f_texcoord;

void main(void) {
  	vec2 texcoord = f_texcoord;

  	float sample1 = texture2D(texture1, texcoord).x;
  	float sample2 = texture2D(texture2, texcoord).x;
	
	//Lower value means closer to the screen: we want to keep the closest of all pixels
	if(sample1 > sample2)
  		gl_FragColor = vec4(sample2,0,0,1);
	if(sample2 >= sample1)
		gl_FragColor = vec4(sample1,0,0,1);


  
}
