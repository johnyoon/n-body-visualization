uniform sampler2D texture1;
uniform sampler2D texture2;
varying vec2 f_texcoord;

void main(void) {
  	vec2 texcoord = f_texcoord;

  	float sample1 = texture2D(texture1, texcoord).x;
  	float sample2 = texture2D(texture2, texcoord).x;

	//Black if nothing there	
  	if(sample1 == 1.0 && sample2 == 1.0)
		gl_FragColor = vec4(0,0,0,1);
	
  	else if(sample1 > sample2)
  		gl_FragColor = vec4(1.0 - 0.2*sample2,1.0 - 0.2*sample2, 1.0 - 0.2*sample2,1);
	else if(sample2 >= sample1)
  		gl_FragColor = vec4(1.0 - 0.2*sample1,1.0 - 0.2*sample1, 1.0 - 0.2*sample1, 1);

}
